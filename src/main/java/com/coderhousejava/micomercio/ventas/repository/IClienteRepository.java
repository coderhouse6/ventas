package com.coderhousejava.micomercio.ventas.repository;

import com.coderhousejava.micomercio.ventas.domain.ClienteDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClienteRepository extends JpaRepository<ClienteDomain, String> {

    ClienteDomain findBycliDni(String nroDniCliente);

}
