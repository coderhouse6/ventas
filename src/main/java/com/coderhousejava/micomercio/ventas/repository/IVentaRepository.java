package com.coderhousejava.micomercio.ventas.repository;

import com.coderhousejava.micomercio.ventas.domain.VentaDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IVentaRepository extends JpaRepository<VentaDomain, String> {

    VentaDomain findByventCodigo(String codVenta);

}
