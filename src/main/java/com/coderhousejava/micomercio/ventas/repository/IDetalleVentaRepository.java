package com.coderhousejava.micomercio.ventas.repository;

import com.coderhousejava.micomercio.ventas.domain.DetalleVentaDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDetalleVentaRepository extends JpaRepository<DetalleVentaDomain, String> {
}
