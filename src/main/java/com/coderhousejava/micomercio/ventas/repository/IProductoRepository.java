package com.coderhousejava.micomercio.ventas.repository;

import com.coderhousejava.micomercio.ventas.domain.ProductoDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductoRepository extends JpaRepository<ProductoDomain, String> {

    ProductoDomain findByprodCodigo(String codProducto);

}
