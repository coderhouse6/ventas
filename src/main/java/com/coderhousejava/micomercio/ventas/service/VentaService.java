package com.coderhousejava.micomercio.ventas.service;

import com.coderhousejava.micomercio.ventas.domain.ClienteDomain;
import com.coderhousejava.micomercio.ventas.domain.DetalleVentaDomain;
import com.coderhousejava.micomercio.ventas.domain.ProductoDomain;
import com.coderhousejava.micomercio.ventas.domain.VentaDomain;
import com.coderhousejava.micomercio.ventas.dto.*;
import com.coderhousejava.micomercio.ventas.exception.ExceptionCustomNotFound;
import com.coderhousejava.micomercio.ventas.feing.IVentasImplement;
import com.coderhousejava.micomercio.ventas.feing.IWorldClockImplement;
import com.coderhousejava.micomercio.ventas.implement.IVentaImplement;
import com.coderhousejava.micomercio.ventas.mapper.IClienteMapper;
import com.coderhousejava.micomercio.ventas.mapper.IVentaMapper;
import com.coderhousejava.micomercio.ventas.repository.IClienteRepository;
import com.coderhousejava.micomercio.ventas.repository.IDetalleVentaRepository;
import com.coderhousejava.micomercio.ventas.repository.IProductoRepository;
import com.coderhousejava.micomercio.ventas.repository.IVentaRepository;
import feign.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class VentaService implements IVentaImplement {

    @Autowired
    private IVentaRepository iVentaRepository;

    @Autowired
    private IVentaMapper iVentaMapper;

    @Autowired
    private IClienteRepository iClienteRepository;

    @Autowired
    private IProductoRepository iProductoRepository;

    @Autowired
    private IWorldClockImplement iWorldClockImplement;

    @Autowired
    private IVentasImplement iVentasImplement;

    @Autowired
    private IDetalleVentaRepository iDetalleVentaRepository;

    @Override
    public List<VentaResponseDTO> findAll() {
        List<VentaDomain> findAll = this.iVentaRepository.findAll();
        if(findAll.isEmpty()) {
            throw new ExceptionCustomNotFound("No se encontraron ventas");
        }
        return this.iVentaMapper.lstVentaDomainToResponse(findAll);
    }

    @Override
    public VentaResponseDTO findByventCodigo(String codVenta) throws Exception {
        Optional<VentaDomain> findByventCodigo = Optional.ofNullable(this.iVentaRepository.findByventCodigo(codVenta));
        if(!findByventCodigo.isPresent()){
            throw new ExceptionCustomNotFound("No existe venta con dicho código ".concat(codVenta));
        }
        return this.iVentaMapper.ventaDomainToResponse(findByventCodigo.get());
    }

    @Override
    public VentaResponseDTO createVenta(VentaRequestCreateDTO ventaRequestCreateDTO) throws Exception {
        Optional<VentaDomain> findByventCodigo = Optional.ofNullable(this.iVentaRepository.findByventCodigo(ventaRequestCreateDTO.getVentCodigoReq()));
        if(findByventCodigo.isPresent()){
            throw new ExceptionCustomNotFound("Existe venta con dicho código ".concat(ventaRequestCreateDTO.getVentCodigoReq()));
        }

        ClienteResponseDTO findClienteByDni = this.iVentasImplement.findClienteByDni(ventaRequestCreateDTO.getCliDniReq());
        ClienteDomain cliDom = this.iClienteRepository.findBycliDni(findClienteByDni.getCliDniResp());

        VentaDomain vent = new VentaDomain();
        vent.setVentId(UUID.randomUUID().toString());
        vent.setClienteDomain(cliDom);
        vent.setVentCodigo(ventaRequestCreateDTO.getVentCodigoReq());
        vent.setVentFecha(this.iWorldClockImplement.getDate().getCurrentDateTime());
        vent.setVentMontoTotal(BigDecimal.valueOf(0.0));

        VentaDomain saveVenta = this.iVentaRepository.save(vent);

        BigDecimal montoTotal = BigDecimal.valueOf(0.0);
        List<DetalleVentaDomain> lstDetVent = new ArrayList<>();

        for(DetalleVentaRequestCreateDTO det : ventaRequestCreateDTO.getDetalleVentaListReq()) {
            ProductoResponseDTO findProductoByCodigo = this.iVentasImplement.findProductoByCodigo(det.getDProdCodigoReq());
            ProductoDomain prodDom = this.iProductoRepository.findByprodCodigo(findProductoByCodigo.getProdCodigoResp());

            if(prodDom.getProdStock().compareTo(BigDecimal.valueOf(0.0)) == -1 || prodDom.getProdStock().compareTo(BigDecimal.valueOf(0.0)) == 0) {
                throw new ExceptionCustomNotFound("No hay stock para el producto con código ".concat(det.getDProdCodigoReq()));
            }

            DetalleVentaDomain detVent = new DetalleVentaDomain();
            detVent.setDVentId(UUID.randomUUID().toString());
            detVent.setVentaDomain(saveVenta);
            detVent.setProductoDomain(prodDom);
            detVent.setDVentCantidad(det.getDVentCantidadReq());
            detVent.setDVentPrecio(findProductoByCodigo.getProdPrecioResp());
            DetalleVentaDomain saveDetVenta = this.iDetalleVentaRepository.save(detVent);

            prodDom.setProdStock(prodDom.getProdStock().subtract(BigDecimal.valueOf(det.getDVentCantidadReq())));
            this.iProductoRepository.save(prodDom);

            lstDetVent.add(saveDetVenta);
            montoTotal = montoTotal.add(findProductoByCodigo.getProdPrecioResp().multiply(BigDecimal.valueOf(det.getDVentCantidadReq())));
        }
        saveVenta.setVentMontoTotal(montoTotal);
        saveVenta.setLstVentDetalle(lstDetVent);
        saveVenta = this.iVentaRepository.save(saveVenta);

        return this.iVentaMapper.ventaDomainToResponse(saveVenta);
    }

    @Override
    public InformacionMensaje deleteVentaByCodigo(String codVenta) throws Exception {
        Optional<VentaDomain> findByventCodigo = Optional.ofNullable(this.iVentaRepository.findByventCodigo(codVenta));
        if(!findByventCodigo.isPresent()) {
            throw new ExceptionCustomNotFound("No existe venta a eliminar con dicho código ".concat(codVenta));
        }
        this.iVentaRepository.delete(findByventCodigo.get());
        InformacionMensaje informacionMensaje = new InformacionMensaje();
        informacionMensaje.setMensaje("Se eliminó la venta correctamente");
        return informacionMensaje;
    }

}
