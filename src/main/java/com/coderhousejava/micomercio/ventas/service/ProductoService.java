package com.coderhousejava.micomercio.ventas.service;

import com.coderhousejava.micomercio.ventas.domain.ProductoDomain;
import com.coderhousejava.micomercio.ventas.dto.*;
import com.coderhousejava.micomercio.ventas.exception.ExceptionCustomError;
import com.coderhousejava.micomercio.ventas.exception.ExceptionCustomNotFound;
import com.coderhousejava.micomercio.ventas.implement.IProductoImplement;
import com.coderhousejava.micomercio.ventas.mapper.IProductoMapper;
import com.coderhousejava.micomercio.ventas.repository.IProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService implements IProductoImplement {

    @Autowired
    private IProductoRepository iProductoRepository;

    @Autowired
    private IProductoMapper iProductoMapper;

    @Override
    public List<ProductoResponseDTO> findAll() {
        List<ProductoDomain> findAll = this.iProductoRepository.findAll();
        if(findAll.isEmpty()) {
            throw new ExceptionCustomNotFound("No se encontraron productos");
        }
        return this.iProductoMapper.lstProductoDomainToResponse(findAll);
    }

    @Override
    public ProductoResponseDTO findProductoByCodigo(String codProducto) throws Exception {
        Optional<ProductoDomain> findByprodCodigo = Optional.ofNullable(this.iProductoRepository.findByprodCodigo(codProducto));
        if(!findByprodCodigo.isPresent()){
            throw new ExceptionCustomNotFound("No existe producto con dicho código ".concat(codProducto));
        }
        return this.iProductoMapper.productoDomainToResponse(findByprodCodigo.get());
    }

    @Override
    public ProductoResponseDTO createProducto(ProductoRequestCreateDTO productoRequestCreateDTO) throws Exception {
        Optional<ProductoDomain> findByprodCodigo = Optional.ofNullable(this.iProductoRepository.findByprodCodigo(productoRequestCreateDTO.getProdCodigoReq()));
        if(findByprodCodigo.isPresent()){
            throw new ExceptionCustomError("Existe un producto con dicho código ".concat(productoRequestCreateDTO.getProdCodigoReq()));
        }
        ProductoDomain productoRequestCreateDTOToDomain = this.iProductoMapper.productoRequestCreateDTOToDomain(productoRequestCreateDTO);
        return this.iProductoMapper.productoDomainToResponse(this.iProductoRepository.save(productoRequestCreateDTOToDomain));
    }

    @Override
    public ProductoResponseDTO updateProducto(ProductoRequestUpdateDTO productoRequestUpdateDTO) throws Exception {
        Optional<ProductoDomain> findById = this.iProductoRepository.findById(productoRequestUpdateDTO.getProdIdResp());
        if(!findById.isPresent()){
            throw new ExceptionCustomNotFound("No existe producto con dicho id");
        }
        ProductoDomain productoRequestUpdateDTOToDomain = this.iProductoMapper.productoRequestUpdateDTOToDomain(productoRequestUpdateDTO);
        productoRequestUpdateDTOToDomain.setProdCodigo(findById.get().getProdCodigo());
        productoRequestUpdateDTOToDomain.setProdStock(findById.get().getProdStock());
        return this.iProductoMapper.productoDomainToResponse(this.iProductoRepository.save(productoRequestUpdateDTOToDomain));
    }

    @Override
    public ProductoResponseDTO updateProductoStock(ProductoRequestUpdateStock productoRequestUpdateStock) throws Exception {
        Optional<ProductoDomain> findByprodCodigo = Optional.ofNullable(this.iProductoRepository.findByprodCodigo(productoRequestUpdateStock.getProdCodigoReq()));
        if(!findByprodCodigo.isPresent()){
            throw new ExceptionCustomNotFound("No existe producto con dicho código ".concat(productoRequestUpdateStock.getProdCodigoReq()));
        }
        findByprodCodigo.get().setProdStock(findByprodCodigo.get().getProdStock().add(productoRequestUpdateStock.getProdStockReq()));
        return this.iProductoMapper.productoDomainToResponse(this.iProductoRepository.save(findByprodCodigo.get()));
    }

    @Override
    public InformacionMensaje deleteProductoByCodigo(String codProducto) throws Exception {
        Optional<ProductoDomain> findByprodCodigo = Optional.ofNullable(this.iProductoRepository.findByprodCodigo(codProducto));
        if(!findByprodCodigo.isPresent()) {
            throw new ExceptionCustomNotFound("No existe producto a eliminar con dicho código ".concat(codProducto));
        }
        this.iProductoRepository.delete(findByprodCodigo.get());
        InformacionMensaje informacionMensaje = new InformacionMensaje();
        informacionMensaje.setMensaje("Se eliminó el producto correctamente");
        return informacionMensaje;
    }

}
