package com.coderhousejava.micomercio.ventas.service;

import com.coderhousejava.micomercio.ventas.domain.ClienteDomain;
import com.coderhousejava.micomercio.ventas.dto.ClienteRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.dto.ClienteRequestUpdatetDTO;
import com.coderhousejava.micomercio.ventas.dto.ClienteResponseDTO;
import com.coderhousejava.micomercio.ventas.dto.InformacionMensaje;
import com.coderhousejava.micomercio.ventas.exception.ExceptionCustomError;
import com.coderhousejava.micomercio.ventas.exception.ExceptionCustomNotFound;
import com.coderhousejava.micomercio.ventas.implement.IClienteImplement;
import com.coderhousejava.micomercio.ventas.mapper.IClienteMapper;
import com.coderhousejava.micomercio.ventas.repository.IClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService implements IClienteImplement {

    @Autowired
    private IClienteRepository iClienteRepository;

    @Autowired
    private IClienteMapper iClienteMapper;

    @Override
    public List<ClienteResponseDTO> findAll() {
        List<ClienteDomain> findAll = this.iClienteRepository.findAll();
        if(findAll.isEmpty()) {
            throw new ExceptionCustomNotFound("No se encontraron clientes");
        }
        return this.iClienteMapper.lstClienteDomainToResponse(findAll);
    }

    @Override
    public ClienteResponseDTO findClienteByDni(String nroDniCliente) throws Exception {
        Optional<ClienteDomain> findClienteByDni = Optional.ofNullable(this.iClienteRepository.findBycliDni(nroDniCliente));
        if(!findClienteByDni.isPresent()){
            throw new ExceptionCustomNotFound("No existe cliente con dicho dni ".concat(nroDniCliente));
        }
        return this.iClienteMapper.clienteDomainToResponse(findClienteByDni.get());
    }

    @Override
    public ClienteResponseDTO createCliente(ClienteRequestCreateDTO clienteRequestCreateDTO) throws Exception {
        Optional<ClienteDomain> findClienteByDni = Optional.ofNullable(this.iClienteRepository.findBycliDni(clienteRequestCreateDTO.getCliDniReq()));
        if(findClienteByDni.isPresent()){
            throw new ExceptionCustomError("Existe un cliente con dicho dni ".concat(clienteRequestCreateDTO.getCliDniReq()));
        }
        ClienteDomain clienteRequestCreateDTOToDomain = this.iClienteMapper.clienteRequestCreateDTOToDomain(clienteRequestCreateDTO);
        return this.iClienteMapper.clienteDomainToResponse(this.iClienteRepository.save(clienteRequestCreateDTOToDomain));
    }

    @Override
    public ClienteResponseDTO updateCliente(ClienteRequestUpdatetDTO clienteRequestUpdatetDTO) throws Exception {
        Optional<ClienteDomain> findById = this.iClienteRepository.findById(clienteRequestUpdatetDTO.getCliIdReq());
        if(!findById.isPresent()){
            throw new ExceptionCustomNotFound("No existe cliente con dicho id");
        }
        ClienteDomain clienteRequestUpdateDTOToDomain = this.iClienteMapper.clienteRequestUpdateDTOToDomain(clienteRequestUpdatetDTO);
        return this.iClienteMapper.clienteDomainToResponse(this.iClienteRepository.save(clienteRequestUpdateDTOToDomain));
    }

    @Override
    public InformacionMensaje deleteClienteByDni(String nroDniCliente) throws Exception {
        Optional<ClienteDomain> findClienteByDni = Optional.ofNullable(this.iClienteRepository.findBycliDni(nroDniCliente));
        if(!findClienteByDni.isPresent()) {
            throw new ExceptionCustomNotFound("No existe cliente a eliminar con dicho dni ".concat(nroDniCliente));
        }
        this.iClienteRepository.delete(findClienteByDni.get());
        InformacionMensaje informacionMensaje = new InformacionMensaje();
        informacionMensaje.setMensaje("Se eliminó el cliente correctamente");
        return informacionMensaje;
    }

}
