package com.coderhousejava.micomercio.ventas.controller;

import com.coderhousejava.micomercio.ventas.dto.VentaRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.implement.IVentaImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/venta")
public class VentaController {

    @Autowired
    private IVentaImplement iVentaImplement;

    @GetMapping(value = "/findAll", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findAll() throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iVentaImplement.findAll());
    }

    @GetMapping(value = "/findVentaByCodigo/{codVenta}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findProductoByCodigo(@PathVariable String codVenta) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iVentaImplement.findByventCodigo(codVenta));
    }

    @PostMapping(value = "/createVenta", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> createVenta(@RequestBody @Valid VentaRequestCreateDTO ventaRequestCreateDTO) throws Exception{
        return ResponseEntity.status(HttpStatus.CREATED).body(this.iVentaImplement.createVenta(ventaRequestCreateDTO));
    }

    @DeleteMapping(value = "/deleteVentaByCodigo/{codVenta}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> deleteVentaByCodigo(@PathVariable String codVenta) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iVentaImplement.deleteVentaByCodigo(codVenta));
    }
}
