package com.coderhousejava.micomercio.ventas.controller;

import com.coderhousejava.micomercio.ventas.dto.ProductoRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.dto.ProductoRequestUpdateDTO;
import com.coderhousejava.micomercio.ventas.dto.ProductoRequestUpdateStock;
import com.coderhousejava.micomercio.ventas.implement.IProductoImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/producto")
public class ProductoController {

    @Autowired
    private IProductoImplement iProductoImplement;

    @GetMapping(value = "/findAll", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findAll() throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iProductoImplement.findAll());
    }

    @GetMapping(value = "/findProductoByCodigo/{codProducto}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findProductoByCodigo(@PathVariable String codProducto) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iProductoImplement.findProductoByCodigo(codProducto));
    }

    @PostMapping(value = "/createProducto", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> createProducto(@RequestBody ProductoRequestCreateDTO productoRequestCreateDTO) throws Exception{
        return ResponseEntity.status(HttpStatus.CREATED).body(this.iProductoImplement.createProducto(productoRequestCreateDTO));
    }

    @PutMapping(value = "/updateProducto", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> updateProducto(@RequestBody ProductoRequestUpdateDTO productoRequestUpdateDTO) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iProductoImplement.updateProducto(productoRequestUpdateDTO));
    }

    @PatchMapping(value = "/updateProductoStock", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> updateProductoStock(@RequestBody ProductoRequestUpdateStock productoRequestUpdateStock) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iProductoImplement.updateProductoStock(productoRequestUpdateStock));
    }

    @DeleteMapping(value = "/deleteProductoByCodigo/{codProducto}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> deleteProductoByCodigo(@PathVariable String codProducto) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iProductoImplement.deleteProductoByCodigo(codProducto));
    }

}
