package com.coderhousejava.micomercio.ventas.controller;

import com.coderhousejava.micomercio.ventas.dto.ClienteRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.dto.ClienteRequestUpdatetDTO;
import com.coderhousejava.micomercio.ventas.implement.IClienteImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

    @Autowired
    private IClienteImplement iClienteImplement;

    @GetMapping(value = "/findAll", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findAll() throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iClienteImplement.findAll());
    }

    @GetMapping(value = "/findClienteByDni/{nroDniCliente}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> findClienteByDni(@PathVariable String nroDniCliente) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iClienteImplement.findClienteByDni(nroDniCliente));
    }

    @PostMapping(value = "/createCliente", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> createCliente(@RequestBody ClienteRequestCreateDTO clienteRequestDTO) throws Exception{
        return ResponseEntity.status(HttpStatus.CREATED).body(this.iClienteImplement.createCliente(clienteRequestDTO));
    }

    @PutMapping(value = "/updateCliente", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> updateCliente(@RequestBody ClienteRequestUpdatetDTO clienteRequestUpdatetDTO) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iClienteImplement.updateCliente(clienteRequestUpdatetDTO));
    }

    @DeleteMapping(value = "/deleteClienteByDni/{nroDniCliente}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> deleteClienteByDni(@PathVariable String nroDniCliente) throws Exception{
        return ResponseEntity.status(HttpStatus.OK).body(this.iClienteImplement.deleteClienteByDni(nroDniCliente));
    }

}
