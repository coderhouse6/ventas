package com.coderhousejava.micomercio.ventas.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbproducto")
public class ProductoDomain {

    @Id
    @Column(name = "prod_id")
    private String prodId;

    @Column(name = "prod_codigo")
    @NotNull(message = "Ingrese nombre del producto")
    private String prodCodigo;

    @Column(name = "prod_descripcion")
    @NotNull(message = "Ingrese descripción del producto")
    private String prodDescripcion;

    @Column(name = "prod_precio")
    @NotNull(message = "Ingrese precio del producto")
    private BigDecimal prodPrecio;

    @Column(name = "prod_stock")
    @NotNull(message = "Ingrese stock del producto")
    private BigDecimal prodStock;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="prod_id")
    private List<DetalleVentaDomain> lstVentDetalle;

}
