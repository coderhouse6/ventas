package com.coderhousejava.micomercio.ventas.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbdetalleventa")
public class DetalleVentaDomain {

    @Id
    @Column(name = "dvent_id")
    private String dVentId;

    @Column(name = "dvent_cantidad")
    private Integer dVentCantidad;

    @Column(name = "dvent_precio")
    private BigDecimal dVentPrecio;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="vent_id")
    private VentaDomain ventaDomain;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="prod_id")
    private ProductoDomain productoDomain;

}
