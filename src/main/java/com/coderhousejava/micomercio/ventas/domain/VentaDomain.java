package com.coderhousejava.micomercio.ventas.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbventa")
public class VentaDomain {

    @Id
    @Column(name = "vent_id")
    private String ventId;

    @Column(name = "vent_codigo")
    private String ventCodigo;

    @Column(name = "vent_fecha")
    private LocalDate ventFecha;

    @Column(name = "vent_monto_total")
    private BigDecimal ventMontoTotal;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name="vent_id")
    private List<DetalleVentaDomain> lstVentDetalle;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="cli_id")
    private ClienteDomain clienteDomain;

}
