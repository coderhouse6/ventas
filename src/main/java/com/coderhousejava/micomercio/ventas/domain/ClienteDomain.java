package com.coderhousejava.micomercio.ventas.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbcliente")
public class ClienteDomain {

    @Id
    @Column(name = "cli_id")
    private String cliId;

    @Column(name = "cli_nombre")
    @NotNull(message = "Ingrese nombre del cliente")
    private String cliNombre;

    @Column(name = "cli_apellido")
    @NotNull(message = "Ingrese apellido del cliente")
    private String cliApellido;

    @Column(name = "cli_dni")
    @NotNull(message = "Ingrese dni del cliente")
    private String cliDni;

    @Column(name = "cli_fecha_nacimiento")
    @NotNull(message = "Ingrese fecha de nacimiento del cliente")
    private LocalDate cliFechaNacimiento;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="cli_id")
    private List<VentaDomain> lstVenta;

}
