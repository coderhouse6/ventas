package com.coderhousejava.micomercio.ventas.feing;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "worldclockapi", url = "http://worldclockapi.com/api/json/utc/now")
public interface IWorldClockImplement {

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    WorldClockDTO getDate();

}
