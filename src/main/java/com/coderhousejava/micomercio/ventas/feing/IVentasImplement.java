package com.coderhousejava.micomercio.ventas.feing;

import com.coderhousejava.micomercio.ventas.dto.ClienteResponseDTO;
import com.coderhousejava.micomercio.ventas.dto.ProductoResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "ventas", url = "http://localhost:8090/api")
public interface IVentasImplement {

    @GetMapping(value = "/cliente/findClienteByDni/{nroDniCliente}", produces = {MediaType.APPLICATION_JSON_VALUE})
    ClienteResponseDTO findClienteByDni(@PathVariable String nroDniCliente);

    @GetMapping(value = "/producto/findProductoByCodigo/{codProducto}", produces = {MediaType.APPLICATION_JSON_VALUE})
    ProductoResponseDTO findProductoByCodigo(@PathVariable String codProducto);

}
