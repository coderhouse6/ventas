package com.coderhousejava.micomercio.ventas.feing;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
public class WorldClockDTO {

        private String $id;
        private LocalDate currentDateTime;
        private LocalTime utcOffset;
        private Boolean isDayLightSavingsTime;
        private String dayOfTheWeek;
        private String timeZoneName;
        private Long currentFileTime;
        private String ordinalDate;
        private String serviceResponse;

}
