package com.coderhousejava.micomercio.ventas.implement;

import com.coderhousejava.micomercio.ventas.dto.*;

import java.util.List;

public interface IProductoImplement {

    List<ProductoResponseDTO> findAll();
    ProductoResponseDTO findProductoByCodigo(String codProducto) throws Exception;
    ProductoResponseDTO createProducto(ProductoRequestCreateDTO productoRequestCreateDTO) throws Exception;
    ProductoResponseDTO updateProducto(ProductoRequestUpdateDTO productoRequestUpdateDTO) throws Exception;
    ProductoResponseDTO updateProductoStock(ProductoRequestUpdateStock productoRequestUpdateStock) throws Exception;
    InformacionMensaje deleteProductoByCodigo(String codProducto) throws Exception;

}
