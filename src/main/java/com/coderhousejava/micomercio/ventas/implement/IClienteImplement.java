package com.coderhousejava.micomercio.ventas.implement;

import com.coderhousejava.micomercio.ventas.dto.ClienteRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.dto.ClienteRequestUpdatetDTO;
import com.coderhousejava.micomercio.ventas.dto.ClienteResponseDTO;
import com.coderhousejava.micomercio.ventas.dto.InformacionMensaje;

import java.util.List;

public interface IClienteImplement {

    List<ClienteResponseDTO> findAll();
    ClienteResponseDTO findClienteByDni(String nroDniCliente) throws Exception;
    ClienteResponseDTO createCliente(ClienteRequestCreateDTO clienteRequestCreateDTO) throws Exception;
    ClienteResponseDTO updateCliente(ClienteRequestUpdatetDTO clienteRequestUpdatetDTO) throws Exception;
    InformacionMensaje deleteClienteByDni(String nroDniCliente) throws Exception;

}
