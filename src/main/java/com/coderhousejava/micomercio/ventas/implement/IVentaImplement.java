package com.coderhousejava.micomercio.ventas.implement;

import com.coderhousejava.micomercio.ventas.dto.InformacionMensaje;
import com.coderhousejava.micomercio.ventas.dto.VentaRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.dto.VentaResponseDTO;

import java.util.List;

public interface IVentaImplement {

    List<VentaResponseDTO> findAll();
    VentaResponseDTO findByventCodigo(String codVenta) throws Exception;
    VentaResponseDTO createVenta(VentaRequestCreateDTO ventaRequestCreateDTO) throws Exception;
    InformacionMensaje deleteVentaByCodigo(String codVenta) throws Exception;

}
