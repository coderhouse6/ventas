package com.coderhousejava.micomercio.ventas.mapper;

import com.coderhousejava.micomercio.ventas.domain.ClienteDomain;
import com.coderhousejava.micomercio.ventas.dto.ClienteRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.dto.ClienteRequestUpdatetDTO;
import com.coderhousejava.micomercio.ventas.dto.ClienteResponseDTO;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring", imports = UUID.class)
public interface IClienteMapper {

    @Mapping(source = "cliId", target = "cliIdResp")
    @Mapping(source = "cliNombre", target = "cliNombreResp")
    @Mapping(source = "cliApellido", target = "cliApellidoResp")
    @Mapping(source = "cliDni", target = "cliDniResp")
    @Mapping(source = "cliFechaNacimiento", target = "cliFechaNacimientoResp")
    ClienteResponseDTO clienteDomainToResponse(ClienteDomain clienteDomain);

    @Mapping(source = "cliId", target = "cliIdResp")
    @Mapping(source = "cliNombre", target = "cliNombreResp")
    @Mapping(source = "cliApellido", target = "cliApellidoResp")
    @Mapping(source = "cliDni", target = "cliDniResp")
    @Mapping(source = "cliFechaNacimiento", target = "cliFechaNacimientoResp")
    List<ClienteResponseDTO> lstClienteDomainToResponse(List<ClienteDomain> clienteDomainList);

    @Mapping(target = "cliId", expression = "java(UUID.randomUUID().toString())")
    @Mapping(target = "cliNombre", source = "cliNombreReq")
    @Mapping(target = "cliApellido", source = "cliApellidoReq")
    @Mapping(target = "cliDni", source = "cliDniReq")
    @Mapping(target = "cliFechaNacimiento", source = "cliFechaNacimientoReq")
    ClienteDomain clienteRequestCreateDTOToDomain(ClienteRequestCreateDTO clienteRequestCreateDTO);

    @Mapping(target = "cliId", source = "cliIdReq")
    @Mapping(target = "cliNombre", source = "cliNombreReq")
    @Mapping(target = "cliApellido", source = "cliApellidoReq")
    @Mapping(target = "cliDni", source = "cliDniReq")
    @Mapping(target = "cliFechaNacimiento", source = "cliFechaNacimientoReq")
    ClienteDomain clienteRequestUpdateDTOToDomain(ClienteRequestUpdatetDTO clienteRequestUpdatetDTO);

    @BeforeMapping
    default void calculoEdad(ClienteDomain clienteDomain, @MappingTarget ClienteResponseDTO clienteResponseDTO) {
        LocalDate fechaNacimiento = clienteDomain.getCliFechaNacimiento();
        Integer edad = (int) ChronoUnit.YEARS.between(fechaNacimiento, LocalDate.now());
        clienteResponseDTO.setCliEdadResp(edad);
    }
}
