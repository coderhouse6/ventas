package com.coderhousejava.micomercio.ventas.mapper;

import com.coderhousejava.micomercio.ventas.domain.ClienteDomain;
import com.coderhousejava.micomercio.ventas.domain.DetalleVentaDomain;
import com.coderhousejava.micomercio.ventas.domain.ProductoDomain;
import com.coderhousejava.micomercio.ventas.domain.VentaDomain;
import com.coderhousejava.micomercio.ventas.dto.*;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring", imports = {UUID.class})
public interface IVentaMapper {

    @Mapping(source = "ventId", target = "ventIdResp")
    @Mapping(source = "ventCodigo", target = "ventCodigoResp")
    @Mapping(source = "ventFecha", target = "ventFechaResp")
    @Mapping(source = "ventMontoTotal", target = "ventMontoTotalResp")
    @Mapping(source = "clienteDomain", target = "clienteResp")
    @Mapping(source = "lstVentDetalle", target = "detalleVentaResp")
    VentaResponseDTO ventaDomainToResponse(VentaDomain ventaDomain);

    @Mapping(source = "ventId", target = "ventIdResp")
    @Mapping(source = "ventCodigo", target = "ventCodigoResp")
    @Mapping(source = "ventFecha", target = "ventFechaResp")
    @Mapping(source = "ventMontoTotal", target = "ventMontoTotalResp")
    @Mapping(source = "clienteDomain", target = "clienteResp")
    @Mapping(source = "lstVentDetalle", target = "detalleVentaResp")
    List<VentaResponseDTO> lstVentaDomainToResponse(List<VentaDomain> ventaDomain);

    @Mapping(source = "DVentId", target = "DVentIdResp")
    @Mapping(source = "productoDomain", target = "productoResp")
    @Mapping(source = "DVentCantidad", target = "DVentCantidadResp")
    @Mapping(source = "DVentPrecio", target = "DVentPrecioResp")
    DetalleVentaResponseDTO detalleVentaDomainToResponse(DetalleVentaDomain detalleVentaDomain);

    @Mapping(source = "cliId", target = "cliIdResp")
    @Mapping(source = "cliNombre", target = "cliNombreResp")
    @Mapping(source = "cliApellido", target = "cliApellidoResp")
    @Mapping(source = "cliDni", target = "cliDniResp")
    @Mapping(source = "cliFechaNacimiento", target = "cliFechaNacimientoResp")
    ClienteResponseDTO clienteDomainToResponse(ClienteDomain clienteDomain);

    @Mapping(source = "prodId", target = "prodIdResp")
    @Mapping(source = "prodCodigo", target = "prodCodigoResp")
    @Mapping(source = "prodDescripcion", target = "prodDescripcionResp")
    @Mapping(source = "prodPrecio", target = "prodPrecioResp")
    @Mapping(source = "prodStock", target = "prodStockResp")
    ProductoResponseDTO productoDomainToResponse(ProductoDomain productoDomain);

    /*@Mapping(source = "DVentId", target = "DVentIdResp")
    @Mapping(source = "ventaDomain.ventId", target = "ventIdResp")
    @Mapping(source = "productoDomain.prodCodigo", target = "prodCodigoResp")
    @Mapping(source = "productoDomain.prodDescripcion", target = "prodDescripcionResp")
    @Mapping(source = "DVentCantidad", target = "DVentCantidadResp")
    @Mapping(source = "DVentPrecio", target = "DVentPrecioResp")
    List<DetalleVentaResponseDTO> lstDetalleVentaDomainToResponse(List<DetalleVentaDomain> detalleVentaDomainList);*/

    @BeforeMapping
    default void calculoEdad(ClienteDomain clienteDomain, @MappingTarget ClienteResponseDTO clienteResponseDTO) {
        LocalDate fechaNacimiento = clienteDomain.getCliFechaNacimiento();
        Integer edad = (int) ChronoUnit.YEARS.between(fechaNacimiento, LocalDate.now());
        clienteResponseDTO.setCliEdadResp(edad);
    }
}
