package com.coderhousejava.micomercio.ventas.mapper;

import com.coderhousejava.micomercio.ventas.domain.ProductoDomain;
import com.coderhousejava.micomercio.ventas.dto.ProductoRequestCreateDTO;
import com.coderhousejava.micomercio.ventas.dto.ProductoRequestUpdateDTO;
import com.coderhousejava.micomercio.ventas.dto.ProductoResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring", imports = UUID.class)
public interface IProductoMapper {

    @Mapping(source = "prodId", target = "prodIdResp")
    @Mapping(source = "prodCodigo", target = "prodCodigoResp")
    @Mapping(source = "prodDescripcion", target = "prodDescripcionResp")
    @Mapping(source = "prodPrecio", target = "prodPrecioResp")
    @Mapping(source = "prodStock", target = "prodStockResp")
    ProductoResponseDTO productoDomainToResponse(ProductoDomain productoDomain);

    @Mapping(source = "prodId", target = "prodIdResp")
    @Mapping(source = "prodCodigo", target = "prodCodigoResp")
    @Mapping(source = "prodDescripcion", target = "prodDescripcionResp")
    @Mapping(source = "prodPrecio", target = "prodPrecioResp")
    @Mapping(source = "prodStock", target = "prodStockResp")
    List<ProductoResponseDTO> lstProductoDomainToResponse(List<ProductoDomain> productoDomainList);

    @Mapping(target = "prodId", expression = "java(UUID.randomUUID().toString())")
    @Mapping(target = "prodCodigo", source = "prodCodigoReq")
    @Mapping(target = "prodDescripcion", source = "prodDescripcionReq")
    @Mapping(target = "prodPrecio", source = "prodPrecioReq")
    @Mapping(target = "prodStock", source = "prodStockReq")
    ProductoDomain productoRequestCreateDTOToDomain(ProductoRequestCreateDTO productoRequestCreateDTO);

    @Mapping(target = "prodId", source = "prodIdResp")
    @Mapping(target = "prodDescripcion", source = "prodDescripcionReq")
    @Mapping(target = "prodPrecio", source = "prodPrecioReq")
    ProductoDomain productoRequestUpdateDTOToDomain(ProductoRequestUpdateDTO productoRequestUpdatetDTO);

}
