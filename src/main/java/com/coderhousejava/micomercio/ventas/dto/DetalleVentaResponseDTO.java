package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class DetalleVentaResponseDTO {

    private String dVentIdResp;
    private ProductoResponseDTO productoResp;
    private Integer dVentCantidadResp;
    private BigDecimal dVentPrecioResp;

}
