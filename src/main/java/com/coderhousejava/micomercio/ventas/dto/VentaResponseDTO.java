package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class VentaResponseDTO {

    private String ventIdResp;
    private String ventCodigoResp;
    private LocalDate ventFechaResp;
    private BigDecimal ventMontoTotalResp;
    private ClienteResponseDTO clienteResp;
    private List<DetalleVentaResponseDTO> detalleVentaResp;

}
