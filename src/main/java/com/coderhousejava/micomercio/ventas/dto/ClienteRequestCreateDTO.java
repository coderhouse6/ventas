package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ClienteRequestCreateDTO {

    private String cliNombreReq;
    private String cliApellidoReq;
    private String cliDniReq;
    private LocalDate cliFechaNacimientoReq;

}
