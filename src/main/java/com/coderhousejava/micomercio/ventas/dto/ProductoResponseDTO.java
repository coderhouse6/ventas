package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class ProductoResponseDTO {

    private String prodIdResp;
    private String prodCodigoResp;
    private String prodDescripcionResp;
    private BigDecimal prodPrecioResp;
    private BigDecimal prodStockResp;

}
