package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class InformacionMensaje {

    private Object mensaje;

}
