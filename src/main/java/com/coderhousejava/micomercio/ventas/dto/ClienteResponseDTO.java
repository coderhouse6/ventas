package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ClienteResponseDTO {

    private String cliIdResp;
    private String cliNombreResp;
    private String cliApellidoResp;
    private String cliDniResp;
    private LocalDate cliFechaNacimientoResp;
    private Integer cliEdadResp;

}
