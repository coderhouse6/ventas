package com.coderhousejava.micomercio.ventas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class DetalleVentaRequestCreateDTO {

    @JsonProperty("dProdCodigoReq")
    @NotEmpty(message = "ingrese el código del producto")
    private String dProdCodigoReq;

    @JsonProperty("dVentCantidadReq")
    @NotEmpty(message = "ingrese la cantidad")
    private Integer dVentCantidadReq;

}
