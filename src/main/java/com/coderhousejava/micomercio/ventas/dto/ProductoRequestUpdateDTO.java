package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class ProductoRequestUpdateDTO {

    private String prodIdResp;
    private String prodDescripcionReq;
    private BigDecimal prodPrecioReq;

}
