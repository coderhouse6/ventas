package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class VentaRequestCreateDTO {

    @NotEmpty(message = "ingrese código de venta")
    private String ventCodigoReq;

    @NotEmpty(message = "ingrese dni de cliente")
    private String cliDniReq;

    @NotEmpty(message = "ingrese el detalle de venta")
    private List<DetalleVentaRequestCreateDTO> detalleVentaListReq;

}
