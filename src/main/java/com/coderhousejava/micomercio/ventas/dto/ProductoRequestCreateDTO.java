package com.coderhousejava.micomercio.ventas.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class ProductoRequestCreateDTO {

    private String prodCodigoReq;
    private String prodDescripcionReq;
    private BigDecimal prodPrecioReq;
    private BigDecimal prodStockReq;

}
