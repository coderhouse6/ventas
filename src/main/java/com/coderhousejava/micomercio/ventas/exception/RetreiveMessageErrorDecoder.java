package com.coderhousejava.micomercio.ventas.exception;

import com.coderhousejava.micomercio.ventas.dto.InformacionMensaje;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;

import java.io.IOException;
import java.io.InputStream;

public class RetreiveMessageErrorDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        InformacionMensaje message = null;
        try {
            InputStream bodyIs = response.body().asInputStream();
            ObjectMapper mapper = new ObjectMapper();
            message = mapper.readValue(bodyIs, InformacionMensaje.class);
        } catch (IOException e) {
            return new ExceptionCustomError(e.getMessage());
        }
        switch (response.status()) {
            case 404:
                return new ExceptionCustomNotFound((String) message.getMensaje());
            default:
                return errorDecoder.decode(methodKey, response);
        }
    }
}