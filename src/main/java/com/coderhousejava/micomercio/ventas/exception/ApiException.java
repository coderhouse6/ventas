package com.coderhousejava.micomercio.ventas.exception;

import com.coderhousejava.micomercio.ventas.dto.InformacionMensaje;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ApiException {

    @ExceptionHandler(ExceptionCustomNotFound.class)
    public ResponseEntity<Object> HExceptionCustomNotFound(ExceptionCustomNotFound exception){
        InformacionMensaje informacionMensaje = new InformacionMensaje();
        informacionMensaje.setMensaje(exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(informacionMensaje);
    }

    @ExceptionHandler(ExceptionCustomError.class)
    public ResponseEntity<Object> HExceptionCustomError(ExceptionCustomError exception){
        InformacionMensaje informacionMensaje = new InformacionMensaje();
        informacionMensaje.setMensaje(exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(informacionMensaje);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> HConstraintViolationException(ConstraintViolationException exception){
        Map<String, String> errores = new HashMap<>();
        exception.getConstraintViolations().forEach((error) -> errores.put(error.getPropertyPath().toString(), error.getMessage()));
        InformacionMensaje informacionMensaje = new InformacionMensaje();
        informacionMensaje.setMensaje(errores);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(informacionMensaje);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> HIllegalArgumentException(IllegalArgumentException exception){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<Object> HSQLException(SQLException exception){
        InformacionMensaje informacionMensaje = new InformacionMensaje();
        informacionMensaje.setMensaje(exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(informacionMensaje);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Object> HFeignException(FeignException exception){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> HMethodArgumentNotValidException(MethodArgumentNotValidException exception){
        Map<String, String> errores = new HashMap<>();
        exception.getAllErrors().forEach((error) -> errores.put(((FieldError) error).getField(), error.getDefaultMessage()));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errores);
    }

}
