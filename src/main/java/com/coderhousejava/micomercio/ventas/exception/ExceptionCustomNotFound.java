package com.coderhousejava.micomercio.ventas.exception;

public class ExceptionCustomNotFound extends RuntimeException{

    public ExceptionCustomNotFound(String mensaje){
        super(mensaje);
    }

}
