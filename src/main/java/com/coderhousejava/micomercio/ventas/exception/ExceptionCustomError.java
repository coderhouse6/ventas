package com.coderhousejava.micomercio.ventas.exception;

public class ExceptionCustomError extends RuntimeException{

    public ExceptionCustomError(String mensaje){
        super(mensaje);
    }

}
