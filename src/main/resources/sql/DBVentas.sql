drop table if exists tbdetalleventa;
drop table if exists tbventa;
drop table if exists tbcliente;
drop table if exists tbproducto;


create table tbproducto (
	prod_id varchar(36) primary key,
	prod_codigo char(8) unique not null,
	prod_descripcion varchar(100) not null,
	prod_precio decimal(6,2) not null,
	prod_stock int not null
);

create table tbcliente (
	cli_id  varchar(36) default uuid() primary key,
	cli_nombre varchar(50) not null,
	cli_apellido varchar(50) not null,
	cli_dni char(8) unique not null,
	cli_fecha_nacimiento date not null
);

create table tbventa (
	vent_id varchar(36) default uuid() primary key,
	cli_id varchar(36) references tbcliente(cli_id),
	vent_codigo char(8) not null,
	vent_fecha date not null,
	vent_monto_total decimal(6,2) not null
);

create table tbdetalleventa (
	dvent_id varchar(36) default uuid() primary key,
	vent_id varchar(36) references tbventa(vent_id),
	prod_id varchar(36) references tbproducto(prod_id),
	dvent_cantidad int not null,
	dvent_precio decimal(6,2) not null
);

select * from tbproducto;
select * from tbcliente;
select * from tbventa;
select * from tbdetalleventa;